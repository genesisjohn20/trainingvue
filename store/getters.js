export default {
    sample_getters(state) {
        return state.sample_state;
    },
    getters_user_items(state) {
        return state.state_user_items;
    },
    // for user list 
    getters_user_list(state) {
        return state.usersState;
    },

    getters_customer_list(state){
        return state.customersState;
    },

    getters_suppplier_list(state){
        return state.supplierState;
    },

    getters_product_list(state){
        return state.productState;
    },

    getter_deliveries_list(state){
        return state.deliveryState;
    },
    getters_deliveriesTrans_list(state){
        return state.deliveryTransState;
    },

    getters_order_list(state){
        return state.orderState;
    },

    getters_ordertrans_list(state){
        return state.orderTransState;
    }
}