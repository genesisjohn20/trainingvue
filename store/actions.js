import axios from "axios";
export default {
      // Login User
      async loginUser({ commit },{username, password}) {
        console.log("what", this.$axios);
        return await axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/login`,
          data:{
            username,
            password,
          }
        })
          .then((res) => {
            console.log(res);
            return res;
          })
          .catch((err) => err);
      },

      // Load User Details
      async loadUser({ commit }) {
        console.log("what", this.$axios);
        return await axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/user`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
        }
        })
          .then((res) => {
            commit("SET_USERS", res.data.view);
            console.log("test", res);
            return res;
          })
          .catch((err) => err);
      },

      // Insert USER details
      async addUser({ commit },{FName,UserName, UserRole, UserStatus}) {
        console.log("what", this.$axios);
        return await axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/user`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            FName,
            UserRole,
            UserName,
            UserStatus
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_USER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },

       // update user
       async editUser({ commit }, { FName, UserName, UserRole, UserStatus,id}) {
        console.log("testedit = ", FName);
        return await axios({
          method: "PATCH",
          url: `${this.$axios.defaults.baseURL}/user/${id}`,
          headers: {
            Authorization: `Bearer ${localStorage.SecretKey}`,
          },
          data: {
            FName,
            UserName,
            UserRole,
            UserStatus
          },
        })
          .then((res) => {
            console.log("supnew", res);
            
            return res;
          })
          .catch((err) => {
            console.log('userID', id);
             return err});
      },


      // Load Customer
      async loadCustomer({ commit }) {
        console.log("what", this.$axios);
        return await axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/customer`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
        }
        })
          .then((res) => {
            commit("SET_CUSTOMERS", res.data.view);
            console.log("test", res);
            return res;
          })
          .catch((err) => err);
      },

      // Insert Customer
      async addCustomer({ commit },{CusName,CusAddress, CusContact}) {
        console.log("what", this.$axios);
        return await axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/customer`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            CusName,
            CusAddress,
            CusContact,
            
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_CUSTOMER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },

      async editCustomer({ commit },{CusName,  CusAddress, CusContact, id}) {
        console.log("what", this.$axios);
        return await axios({
          method: "PATCH",
          url: `${this.$axios.defaults.baseURL}/supplier/${id}`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            CusName,
            CusAddress,
            CusContact,
            
          }
        })
          .then((res) => {
            console.log("result",res);
            console.log("test");
            // commit("ADD_SUPPLIER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },

      // load Supplier Details
      async loadSupplier({ commit }) {
        console.log("what", this.$axios);
        return await axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/supplier`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
        }
        })
          .then((res) => {
            commit("SET_SUPPLIER", res.data.view);
            console.log("test", res);
            return res;
          })
          .catch((err) => err);
      },

      // Add Supplier
      async addSupplier({ commit },{SuppName,  SuppContact, SuppAddress}) {
        console.log("what", this.$axios);
        return await axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/supplier`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            SuppName,
            SuppContact,
            SuppAddress,
            
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_SUPPLIER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },

      async editSupplier({ commit },{SuppName,  SuppContact, SuppAddress, id}) {
        console.log("what", this.$axios);
        return await axios({
          method: "PATCH",
          url: `${this.$axios.defaults.baseURL}/supplier/${id}`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            SuppName,
            SuppContact,
            SuppAddress,
            
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_SUPPLIER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },


      // load Product details
      async loadProduct({ commit }) {
        console.log("what", this.$axios);
        return await axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/product`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
        }
        })
          .then((res) => {
            commit("SET_PRODUCT", res.data.view);
            console.log("test", res);
            return res;
          })
          .catch((err) => err);
      },

      

      async addProduct({ commit },{ProdDesc, Price, Quantity}) {
        console.log("what", this.$axios);
        return await axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/product`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            
            ProdDesc,
            Price,
            Quantity
            
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_CUSTOMER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },

      async UpdateProductPrice({ commit },{prodID, Price}) {
        // console.log("what", this.$axios);
        console.log(prodID+" - "+Price);
        return await axios({
          method: "PATCH",
          url: `${this.$axios.defaults.baseURL}/product/${prodID}`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            Price,
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_CUSTOMER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },


      async UpdateProduct({ commit },{prodID, ProdDesc, Price, Quantity}) {
        // console.log("what", this.$axios);
        console.log(prodID+" - "+ProdDesc);
        return await axios({
          method: "PATCH",
          url: `${this.$axios.defaults.baseURL}/product/${prodID}`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            ProdDesc,
            Price,
            Quantity
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_CUSTOMER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },

      async addSupply({ commit },{isStatus, UserID,items, date, SuppID, FName, UserRole}) {
        console.log("what", this.$axios);
        return await axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/delivery`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            DateDelivered: date,
            transactions_details: {
              details: items
            },
            isStatus,
            SuppID,
            UserID,
            
            
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_CUSTOMER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },

      async loadDeliveryTrans({ commit },{id}) {
        console.log("what", this.$axios);
        return await axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/deliveryTrans/${id}`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            id
          }
        
        })
          .then((res) => {
            commit("SET_DELIVERYTRANS", res.data.view);
            console.log("test", res);
            return res;
          })
          .catch((err) => err);
      },

      async loadDelivery({ commit }) {
        console.log("what", this.$axios);
        return await axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/delivery`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
        }
        })
          .then((res) => {
            commit("SET_DELIVERY", res.data.view);
            console.log("test", res);
            return res;
          })
          .catch((err) => err);
      },

      async loadOrder({ commit }) {
        console.log("what", this.$axios);
        return await axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/order`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
        }
        })
          .then((res) => {
            commit("SET_ORDER", res.data.view);
            console.log("test", res);
            return res;
          })
          .catch((err) => err);
      },
      
      
      async addOrder({ commit },{date, items, TotalAmount, TenderedAmount, Change, CustomerTBLId, UserTBLId, Trans_Status}) {
        console.log("what", this.$axios);
        return await axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/order`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
          },
          data:{
            TransactionDate: date,
            TransPaymentDetails: {
              details: items
            },
            TotalAmount,
            TenderedAmount,
            Change,
            CustomerTBLId,
            UserTBLId,
            Trans_Status
            
          }
        })
          .then((res) => {
            console.log("result",res);
            // commit("ADD_CUSTOMER", res.data.view)
            return res;
          })
          .catch((err) => err);
      },
      

      async loadOrderTransaction({ commit }) {
        console.log("what", this.$axios);
        return await axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/transPayment`,
          headers:{
            Authorization: `Bearer ${localStorage.SecretKey}` 
        }
        })
          .then((res) => {
            commit("SET_ORDER_TRANSACTION", res.data.view);
            console.log("test", res);
            return res;
          })
          .catch((err) => err);
      },

      

     

      //Update User details
      // async updateUser({coomit}, {})
}