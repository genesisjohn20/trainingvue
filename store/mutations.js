import state from "./state";
export default {
    SET_USERS(state, user) {
        state.usersState = user;
      },
    
    ADD_USER(state, user){
      console.log("user", user);
      state.usersState.push(user);
    },

    EDIT_USER(state, user){
      const index = state.usersState.findIndex();
    //  state.usersState.splice(index, 1);
      state.usersState.push(user);
    },

    SET_CUSTOMERS(state, customer){
      state.customersState = customer;
    },
    ADD_CUSTOMER(state, customer){
      state.customersState.push(customer);
    },

    EDIT_CUSTOMER(state, customer){
      const index = state.customersState.findIndex();
    //  state.usersState.splice(index, 1);
      state.customersState.push(customer);
    },

    SET_SUPPLIER(state, supplier){
      state.supplierState = supplier;
    },

    ADD_SUPPLIER(state, supplier){
      state.supplierState.push(supplier);
    },

    EDIT_SUPPLIER(state, supplier){
      const index = state.supplierState.findIndex();
      state.supplierState.push(supplier);
    },

    SET_PRODUCT(state, product){
      state.productState = product;
    },

    ADD_PRODUCT(state, product){
      state.productState.push(product);
    },

    EDIT_PRODUCT(state, product){
      const index = state.productState.findIndex();
    //  state.usersState.splice(index, 1);
      state.productState.push(product);
    },

    ADD_DELIVERY(state, delivery){
      state.deliveryState.push(delivery);
    },
    
    SET_DELIVERYTRANS(state, deliveryTrans){
      state.deliveryTransState = deliveryTrans;
    },

    SET_DELIVERY(state, delivery){
      state.deliveryState = delivery;
    },

    SET_ORDER(state, order){
      state.orderState = order;
    },

    ADD_ORDER(state, order){
      state.orderState.push(order);
    },

    SET_ORDER_TRANSACTION(state, transPayment){
      state.orderTransState = transPayment;
    }


}